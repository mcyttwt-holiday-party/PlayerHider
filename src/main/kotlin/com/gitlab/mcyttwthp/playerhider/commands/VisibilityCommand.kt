package com.gitlab.mcyttwthp.playerhider.commands

import com.gitlab.mcyttwthp.playerhider.Main
import com.gitlab.mcyttwthp.playerhider.util.Colors
import com.gitlab.mcyttwthp.playerhider.visibility.PlayerVisibility
import com.gitlab.mcyttwthp.playerhider.visibility.datatypes.PlayerVisibilityConfig
import org.bukkit.ChatColor
import org.bukkit.command.Command
import org.bukkit.command.CommandExecutor
import org.bukkit.command.CommandSender
import org.bukkit.command.TabCompleter
import org.bukkit.entity.Player

object VisibilityCommand : CommandExecutor, TabCompleter {
    override fun onCommand(sender: CommandSender, command: Command, label: String, args: Array<out String>): Boolean {
        if (sender !is Player) {
            sender.sendMessage("${Colors.RESPONSE_PREFIX}${ChatColor.RED}You are not a player!")
            return true
        }

        if (args.isEmpty()) return false
        when (args[0].toLowerCase()) {
            "show",
            "hide",
            "toggle" -> {
                if (args.size == 1) return false

                val players = args.slice(1 until args.size)

                if (PlayerVisibility.players[sender.uniqueId] == null) {
                    PlayerVisibility.players[sender.uniqueId] = PlayerVisibilityConfig(
                        hidden = mutableListOf(),
                        ignored = mutableListOf()
                    )
                }

                val conf = PlayerVisibility.players[sender.uniqueId] ?: return true // Silently fail, that way it doesn't brick the entire plugin.

                if (!players.contains("-a")) {
                    // True : Visible, False : Hidden
                    val changed = mutableMapOf<Player, Boolean>()
                    players.forEach {
                        if (
                            !Main.plugin.server.onlinePlayers.any { pl -> pl.name.toLowerCase() == it.toLowerCase() }
                            &&
                            !Main.plugin.server.offlinePlayers.any { pl -> pl.name?.toLowerCase() == it.toLowerCase() }
                        ) {
                            sender.sendMessage("${Colors.RESPONSE_PREFIX}Player ${ChatColor.YELLOW}${it}${ChatColor.RESET} does not exist!")
                            return@forEach
                        }

                        val player = Main.plugin.server.onlinePlayers.find { pl -> pl.name.toLowerCase() == it.toLowerCase() }
                            ?: Main.plugin.server.offlinePlayers.find { pl -> pl.name?.toLowerCase() == it.toLowerCase() }

                        if (player == null) {
                            sender.sendMessage("${Colors.RESPONSE_PREFIX}Player ${ChatColor.YELLOW}${it}${ChatColor.RESET} does not exist!")
                            return@forEach
                        }

                        if (conf.hidden.contains(player.name)) {
                            conf.hidden.remove(player.name)
                            conf.ignored.add(player.name ?: return@forEach)

                            sender.showPlayer(Main.plugin, player.player ?: return@forEach)

                            changed[player.player!!] = true
                        } else {
                            conf.hidden.add(player.name ?: return@forEach)
                            conf.ignored.remove(player.name ?: return@forEach)

                            sender.hidePlayer(Main.plugin, player.player ?: return@forEach)

                            changed[player.player!!] = false
                        }
                    }

                    sender.sendMessage("${Colors.RESPONSE_PREFIX}Toggled visibility for players :")
                    changed.forEach {
                        sender.sendMessage("    - ${ChatColor.YELLOW}${it.key.name} ${ChatColor.AQUA}: ${if (it.value) "${ChatColor.GREEN}Visible" else "${ChatColor.RED}Hidden"}")
                    }
                } else {
                    if (conf.hidden.contains("playerhider.hide_all")) {
                        conf.hidden.remove("playerhider.hide_all")

                        Main.plugin.server.onlinePlayers.forEach {
                            if (it.hasPermission("playerhider.ignore")) return@forEach
                            
                            sender.showPlayer(Main.plugin, it)
                        }

                        sender.sendMessage("${Colors.RESPONSE_PREFIX}Successfully shown back all players!")
                    } else {
                        conf.hidden.add("playerhider.hide_all")

                        Main.plugin.server.onlinePlayers.forEach {
                            if (conf.ignored.contains(it.name))
                                return@forEach

                            if (it.hasPermission("playerhider.ignore")) return@forEach

                            sender.hidePlayer(Main.plugin, it)
                        }

                        sender.sendMessage("${Colors.RESPONSE_PREFIX}Successfully hid all players!")
                    }
                }
            }

            else -> return false
        }

        PlayerVisibility.update()

        return true
    }

    override fun onTabComplete(
        sender: CommandSender,
        command: Command,
        alias: String,
        args: Array<out String>
    ): MutableList<String> {
        val player = sender as Player
        return when (args.size) {
            1 -> mutableListOf("toggle", "hide", "show")
            else -> {
                val list = mutableListOf("-a")
                list.addAll(Main.plugin.server.onlinePlayers.map { it.name }.toMutableList())
                list.addAll(Main.plugin.server.offlinePlayers.map { it.name ?: "" }.toMutableList())
                list.remove(player.name)

                list
            }
        }
    }
}