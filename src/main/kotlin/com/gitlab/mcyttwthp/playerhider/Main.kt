package com.gitlab.mcyttwthp.playerhider

import com.gitlab.mcyttwthp.playerhider.commands.VisibilityCommand
import com.gitlab.mcyttwthp.playerhider.events.PlayerOnJoinHandler
import org.bukkit.plugin.java.JavaPlugin

class Main : JavaPlugin() {
    override fun onEnable() {
        plugin = this

        this.saveDefaultConfig()

        this.getCommand("visibility")?.setExecutor(VisibilityCommand)
        this.getCommand("visibility")?.tabCompleter = VisibilityCommand

        this.server.pluginManager.registerEvents(PlayerOnJoinHandler, this)
    }

    companion object {
        lateinit var plugin: JavaPlugin
    }
}