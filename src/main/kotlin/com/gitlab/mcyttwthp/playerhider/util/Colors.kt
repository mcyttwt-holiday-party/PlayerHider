package com.gitlab.mcyttwthp.playerhider.util

import org.bukkit.ChatColor

object Colors {
    val RESPONSE_PREFIX = "${ChatColor.DARK_AQUA}Player${ChatColor.GRAY}${ChatColor.BOLD}Hider ${ChatColor.AQUA}>> ${ChatColor.RESET}"
}