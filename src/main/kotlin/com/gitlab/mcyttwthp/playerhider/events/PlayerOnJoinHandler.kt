package com.gitlab.mcyttwthp.playerhider.events

import com.gitlab.mcyttwthp.playerhider.Main
import com.gitlab.mcyttwthp.playerhider.util.Colors
import com.gitlab.mcyttwthp.playerhider.visibility.PlayerVisibility
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.player.PlayerJoinEvent

object PlayerOnJoinHandler : Listener {
    @EventHandler
    fun onPlayerJoin(ev: PlayerJoinEvent) {
        val jpVis = PlayerVisibility.players[ev.player.uniqueId]

        Main.plugin.server.onlinePlayers.forEach {
            if (jpVis != null) {
                if (jpVis.hidden.contains(it.uniqueId.toString()) || (jpVis.hidden.contains("playerhider.hide_all") && !it.hasPermission("playerhider.ignore") && !jpVis.ignored.contains(it.uniqueId.toString())))
                    ev.player.hidePlayer(Main.plugin, it)
            }

            val visInfo = PlayerVisibility.players[it.uniqueId] ?: return@forEach

            if (visInfo.hidden.contains(ev.player.uniqueId.toString()) || (visInfo.hidden.contains("playerhider.hide_all") && !it.hasPermission("playerhider.ignore")  && !visInfo.ignored.contains(ev.player.uniqueId.toString())))
                it.hidePlayer(Main.plugin, ev.player)
        }

        if (jpVis?.hidden?.contains("playerhider.hide_all") == true)
            ev.player.sendMessage("${Colors.RESPONSE_PREFIX}All players were automatically hidden for you in order to increase your performance.")
    }
}