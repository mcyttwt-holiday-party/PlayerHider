package com.gitlab.mcyttwthp.playerhider.visibility

import com.gitlab.mcyttwthp.playerhider.Main
import com.gitlab.mcyttwthp.playerhider.visibility.datatypes.PlayerVisibilityConfig
import org.bukkit.configuration.file.YamlConfiguration
import java.io.File
import java.util.*

object PlayerVisibility {
    private val config = if (File(Main.plugin.dataFolder, "players.yml").exists())
        YamlConfiguration.loadConfiguration(File(Main.plugin.dataFolder, "players.yml"))
    else
        YamlConfiguration.loadConfiguration("".reader())
    val players: MutableMap<UUID, PlayerVisibilityConfig> = mutableMapOf()

    init {
        if (config.getKeys(false).isNotEmpty()) {
            config.getKeys(false).forEach {
                val conf = config.getConfigurationSection(it)!!

                players[UUID.fromString(it)] = PlayerVisibilityConfig(
                    hidden = conf.getStringList("hidden"),
                    ignored = conf.getStringList("ignored")
                )
            }
        }

        update()
    }

    fun update() {
        players.forEach {
            if (config.getConfigurationSection(it.key.toString()) == null)
                config.createSection(it.key.toString())

            config.getConfigurationSection(it.key.toString())!!["hidden"] = it.value.hidden
            config.getConfigurationSection(it.key.toString())!!["ignored"] = it.value.ignored
        }

        config.save(File(Main.plugin.dataFolder, "players.yml"))
    }
}