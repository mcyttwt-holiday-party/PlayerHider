package com.gitlab.mcyttwthp.playerhider.visibility.datatypes

data class PlayerVisibilityConfig(
    val hidden: MutableList<String>,
    val ignored: MutableList<String>
)